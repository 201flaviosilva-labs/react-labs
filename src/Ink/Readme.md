# React Ink

- [Official GitHub Ink Project](https://github.com/vadimdemedes/ink);

## Quick App
> npx create-ink-app

(This command will create a new Ink app in the current directory. So create a new folder and run the command.)
