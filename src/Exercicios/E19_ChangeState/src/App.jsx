import React from 'react';
import { Component } from 'react';
import Contador from './components/Contador';


class App extends Component{
    constructor(){
        super();
        this.state = {
            count:0
        }
        // this.handleClickMy = this.handleClickMy.bind(this); // Não percebi o que é isto, mas ya // Acho que faz a ligação entre a função e o butão
    }

    handleClickMy(){
        // this.setState({count:1}); // um pouco estranho, mas come-se
        this.setState(prevState => {
            return{
                count: prevState.count + 1
            }
        });
    }

    render(){
        const { count } = this.state;  // para evitar estar sempre a escrever ->  this.state.count;
        return(
            <>
                <p>Count p: {count}</p>
                <Contador count={count}/> {/* O componente não está a atualizar*/}
                <button type="button"  onClick={() => this.handleClickMy()}> Click </button>
                {/* <button type="button"  onClick={() => this.handleClickMy.bind(this)()}> Click </button> // outra possibilidade */}
                
                {/* this.handleClickMy // como eu vi no video e funciona // é nessecário usar o bind*/}
                {/* () =>  this.handleClickMy() // tambem funciona // não há necessidade de usar o bind*/}
                {/* () =>  this.handleClickMy // não faz nada*/}
                {/* this.handleClickMy() // erro  // é uma variavel*/}
            </>
        )
    }
}

export default App;
