import React from "react";
import { Component } from "react";

class Contador extends Component {
    constructor(props) {
        super(props);
        console.log(props);
        this.state = {
            count: props.count
        }
        console.log(this.state.count);
    }
    render() {
        return (
            <h1>Contador Componente h1: {this.state.count}</h1>
        )
    }

}

export default Contador;