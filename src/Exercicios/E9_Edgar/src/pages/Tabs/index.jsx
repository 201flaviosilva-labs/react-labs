import React, { useReducer, useRef, useLayoutEffect } from "react";
import TabsComp from "../../components/TabComp";
import "./style.css";

const items = ["Page1", "Page2", "Page3"];

function reducer(state, action) {
    switch (action.type) {
        case "reset":
            return { ...state, count: 0 };
        case "increment":
            return { count: state.count + 1, opcoaSelecionada: action.novaOpcao };
        default:
            break;
    }
}

export default function Tabs() {
    const forwardRef = useRef(null);
    const initialState = { opcoaSelecionada: "Page1", count: 0 };
    const [state, dispatch] = useReducer(reducer, initialState);

    useLayoutEffect(() => {
        document.addEventListener("mousedown", event => {
            if (forwardRef.current && !forwardRef.current.contains(event.target)) dispatch({ type: "reset" });
        })
    });

    function image() {
        if (state.opcoaSelecionada === "Page1") {
            return "https://media.giphy.com/media/v98tJD7v68bCw/giphy.gif";
        }
        if (state.opcoaSelecionada === "Page2") {
            return "https://media.giphy.com/media/s1LnlUikzgF6U/giphy.gif";
        }
        if (state.opcoaSelecionada === "Page3") {
            return "https://media.giphy.com/media/EbLhhaLigrEIg/giphy.gif";
        }
    }

    return (
        <div className="TabsContentor">
            <h1>Tabs</h1>
            <TabsComp items={items} selectedOption={items[0]} callback={(opcao) => dispatch({ type: "increment", novaOpcao: opcao })} forwardRef={forwardRef} />
            <p>Número Clicks: {state.count}</p>
            <div className="divIamgem">
                <img src={image()} alt="imagem" />
            </div>
        </div>
    )
}