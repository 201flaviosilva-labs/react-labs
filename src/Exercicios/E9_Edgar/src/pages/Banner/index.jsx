import React from "react";
import BannerComp from "../../components/BannerComp";
import "./style.css";

function BannerPage() {
    return (
        <div className="BannerContentor">
            <h1>Banner</h1>
            <BannerComp bgcolor={"red"}> Perigo! Algo pode acontecer</BannerComp>
        </div>
    )
}

export default BannerPage;