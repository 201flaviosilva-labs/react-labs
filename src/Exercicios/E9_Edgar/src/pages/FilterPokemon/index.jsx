import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import PokeCard from "../../components/PokeCard";

import { getPokemonById, getPokemonID, getCurrentPage } from "../../Store/selectors";
import pokemonAction from "../../Store/actions/pokemonAction";
import navigationAction from "../../Store/actions/navigationAction";

export default function FilterPokemon() {
    return (
        <div className="FilterPokemonContainer">
            <h1> Redux Pokemon Exercise </h1>
                <PokeCard
                    name={pokemonInfo.name}
                    pokemonInfo={{
                        speed: getType("speed"),
                        specialDefense: getType("specialDefense"),
                        specialAttack: getType("specialAttack"),
                        defense: getType("defense"),
                        weight: getType("weight"),
                        height: getType("height"),
                        type: getType("type"),

                        attack: getType("attack"),
                        hp: getType("hp"),
                        ability: getAvailabity("Normal"),
                        hiddenAbility: getAvailabity("Hidden")
                    }}
                    imageSrc={pokemonInfo?.sprites?.front_default} />
                <button><Link to={urlBuilder('previous')} onClick={() => navigate(urlBuilder('previous'))}>Previous</Link></button>
                <button><Link to={urlBuilder('next')} onClick={() => navigate(urlBuilder('next'))}>Next</Link></button>
        </div>
    );
}