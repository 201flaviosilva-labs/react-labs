import React from "react";
import { Link } from "react-router-dom";
import "./style.css";

function Home() {
    return (
        <div className="homeContentor">
            <h1>Home</h1>
            <h2>Alguns exercicios para brincar com o react</h2>
            <h3>Links</h3>
            <section>

                <article>
                    <h4>Imagens</h4>
                    <p>Imagens no React</p>
                    <Link to="/imagens">Imagens</Link>
                </article>

                <article>
                    <h4>Sobre</h4>
                    <p>SweetAlert</p>
                    <Link to="/sobre">Sobre</Link>
                </article>

                <article>
                    <h4>DropDown</h4>
                    <p>Um DropDown cheio de stilo no recat</p>
                    <Link to="/dropdown">DropDown</Link>
                </article>

                <article>
                    <h4>Banner</h4>
                    <p>UseLayoutEffect</p>
                    <Link to="/banner">Banner</Link>
                </article>

                <article>
                    <h4>Tabs</h4>
                    <p>Use Ref</p>
                    <Link to="/tabs">Tabs</Link>
                </article>

                <article>
                    <h4>Fetch</h4>
                    <p>Fetch Api</p>
                    <Link to="/fetch">Fetch</Link>
                </article>

                <article>
                    <h4>ReduxEx</h4>
                    <p>Exercicio Redux</p>
                    <Link to="/reduxExercise">Redux</Link>
                </article>
            </section>
        </div>
    )
}

export default Home;