import React from "react";
import Torre from "../../Assets/img/Torre.jpg";
import "./style.css";

function Imagens() {
    return (
        <div className="imagensContentor">
            <h1>Imagens</h1>
            <p>Apenas uma imagem vinda da net aqui:</p>
            <img src="https://picsum.photos/200/300" title="Imagem Aletória" alt="Imagem Aletória" />
            <p>Uma imagem na source:</p>
            <img src={Torre} title="Torre" alt="Torre" />
        </div>
    )
}

export default Imagens;