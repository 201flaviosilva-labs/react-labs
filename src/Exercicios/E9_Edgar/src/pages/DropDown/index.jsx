import React from "react";
import "./style.css"
import DropDownElemento from "../../components/DropDownComp";

const items = ["Roxo", "Verde", "Preto", "Branco", "Vermelho", "Azul"];
const selected = "Preto";

function DropDown() {
    return (
        <div className="DropDownContentor">
            <h1>Drop Down</h1>
            <DropDownElemento selected={selected} items={items} />
        </div>
    )
}

export default DropDown;