import React from "react";
import swal from 'sweetalert';
import "./style.css";

function Sobre() {
    return (
        <div className="sobreContentor">
            <h1>Sobre</h1>
            <button onClick={() => swal("Um Evento de click!", "Muito bem clicas-te no botão", "success")}>Click</button>
        </div>
    )
}

export default Sobre;