import React, { useEffect, useState } from "react";
import "./style.min.css";

// Imagens
import alakazamImg from "../../Assets/img/alakazam.png";
import blastoiseImg from "../../Assets/img/blastoise.png";
import bulbasaurImg from "../../Assets/img/bulbasaur.png";
import charizardImg from "../../Assets/img/charizard.png";
import charmeleondImg from "../../Assets/img/charmeleon.png";
import charmanderdImg from "../../Assets/img/charmander.png";
import flareondImg from "../../Assets/img/flareon.png";
import gengarImg from "../../Assets/img/gengar.png";
import haunterImg from "../../Assets/img/haunter.png";
import pikachuImg from "../../Assets/img/pikachu.png";
import squirtleImg from "../../Assets/img/squirtle.png";
import vileplumeImg from "../../Assets/img/vileplume.png";
import wartortleImg from "../../Assets/img/wartortle.png";

export default function Fetch() {

    const pokesList = ["alakazam", "blastoise", "bulbasaur", "charizard", "charmeleon", "charmander", "flareon", "gengar", "haunter", "pikachu", "squirtle", "vileplume", "wartortle"];

    const [selectedPokemon, setSelectedPokemon] = useState("charizard");
    const [pokeDados, setPokeDados] = useState("");

    const [pokeAbilTitle, setPokeAbilTitle] = useState("");
    const [pokeAbilDescript, setPokeAbilDescript] = useState("");

    useEffect(() => {
        if (!pokeDados[selectedPokemon]) {
            getPokemonss();
            setPokeAbilTitle("");
            setPokeAbilDescript("");
        }
    }, []);
    function getPokemonss() {
        fetch(`https://pokeapi.co/api/v2/pokemon/${selectedPokemon}`)
            .then((response) => response.json())
            .then((data) => {
                setPokeDados({ ...pokeDados, [selectedPokemon]: data });
            })
    }


    function getImg(selectedPokemon) {
        switch (selectedPokemon) {
            case "alakazam":
                return alakazamImg;
            case "blastoise":
                return blastoiseImg;
            case "bulbasaur":
                return bulbasaurImg;
            case "charizard":
                return charizardImg;
            case "charmeleon":
                return charmeleondImg;
            case "charmander":
                return charmanderdImg;
            case "flareon":
                return flareondImg;
            case "gengar":
                return gengarImg;
            case "haunter":
                return haunterImg;
            case "pikachu":
                return pikachuImg;
            case "squirtle":
                return squirtleImg;
            case "vileplume":
                return vileplumeImg;
            case "wartortle":
                return wartortleImg;
            default:
                return "Error";
        }
    }

    function getType(feature) {
        switch (feature) {
            case "speed":
                return pokeDados[selectedPokemon]?.stats[0]?.base_stat;
            case "specialDefense":
                return pokeDados[selectedPokemon]?.stats[1]?.base_stat;
            case "specialAttack":
                return pokeDados[selectedPokemon]?.stats[2]?.base_stat;
            case "defense":
                return pokeDados[selectedPokemon]?.stats[3]?.base_stat;
            case "attack":
                return pokeDados[selectedPokemon]?.stats[4]?.base_stat;
            case "hp":
                return pokeDados[selectedPokemon]?.stats[5]?.base_stat;
            case "weight":
                return pokeDados[selectedPokemon]?.weight;
            case "height":
                return pokeDados[selectedPokemon]?.height;
            case "type":
                return pokeDados[selectedPokemon]?.types[
                    pokeDados[selectedPokemon]?.types.length - 1
                ]?.type?.name;
            default:
                return;
        }
    }

    function getAvailabity(type) {
        const ability = pokeDados[selectedPokemon]?.abilities.find(
            ability =>
                (type === "Normal" && !ability.is_hidden) ||
                (type === "Hidden" && ability.is_hidden)
        );
        return ability?.ability?.name;
    }

    function getAvailabityDescUrl(type) {
        const ability = pokeDados[selectedPokemon]?.abilities.find(
            ability =>
                (type === "Normal" && !ability.is_hidden) ||
                (type === "Hidden" && ability.is_hidden)
        );
        return ability?.ability?.url;
    }

    function hiddenAbilityFunc(tipo) {
        setPokeAbilTitle(getAvailabity(tipo));
        fetch(`${getAvailabityDescUrl(tipo)}`)
            .then((response) => response.json())
            .then((data) => {
                setPokeAbilDescript(data.effect_entries[0].effect ? data.effect_entries[0].effect : "Nop");
            })
    }

    return (
        <div className="FetchContentor">
            <h1>Fetch Api</h1>
            <section>
                <select value={selectedPokemon} onChange={event => setSelectedPokemon(event.target.value)}>
                    {pokesList.map(e => {
                        return (
                            <option value={e}>{e}</option>
                        )
                    })}
                </select>
            </section>

            <article>
                <div className={getType("type")}>
                    <img src={getImg(selectedPokemon)} title="Pokemon" alt="Pokemon" />
                    <main>
                        <h3>{getType("type")}</h3>
                        <h2>{selectedPokemon}</h2>
                        <ul>
                            <li><span>HP:</span> <span>{getType("hp")}</span></li>
                            <li><span>Attack:</span> <span>{getType("attack")}</span></li>
                            <li><span>Defense:</span> <span> {getType("defense")}</span></li>
                            <li><span>Special Attack:</span> <span> {getType("specialAttack")}</span></li>
                            <li><span>Special Defense: </span> <span> {getType("specialDefense")}</span></li>
                            <li><span>Speed:</span> <span> {getType("speed")}</span></li>
                            <li><span>Height:</span> <span>{getType("height")}</span></li>
                            <li><span>Weight:</span> <span> {getType("weight")}</span></li>
                        </ul>
                        <section>
                            <article onClick={() => hiddenAbilityFunc("Normal")}>
                                <h4>Abilidade</h4>
                                <p>{getAvailabity("Normal")}</p>
                            </article>
                            <article onClick={() => hiddenAbilityFunc("Hidden")}>
                                <h4>Hidden Ability</h4>
                                <p>{getAvailabity("Hidden")}</p>
                            </article>
                        </section>
                    </main>
                </div>
                <aside>
                    <h2>{pokeAbilTitle}</h2>
                    <p>{pokeAbilDescript}</p>
                </aside>
            </article>
        </div>
    )
}