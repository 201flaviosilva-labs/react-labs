import React, { useState, useLayoutEffect } from "react";
import "./style.css";

export default function TabsComp({ items, selectedOption, callback, forwardRef }) {
    const [opcoaSelecionada, setOpcoaSelecionada] = useState(selectedOption);

    useLayoutEffect(() => {
        const selectedOption = document.getElementsByClassName("selectedOption")[0];
        const litleBar = document.getElementsByClassName("litleBar")[0];

        const positionLeft = `${selectedOption.offsetLeft}px`;
        const positionTop = selectedOption.offsetTop - 2;
        const tamanho = selectedOption.offsetWidth;

        litleBar.style.left = positionLeft;
        litleBar.style.top = positionTop + "px";
        litleBar.style.width = tamanho + "px";
    });

    return (
        <div className="TabsPagsContentor" ref={forwardRef}>
            <div className="litleBar"></div>
            <ul>
                {items.map(element => (
                    <li onClick={
                        () => {
                            setOpcoaSelecionada(element);
                            callback(element);
                        }}
                        className={opcoaSelecionada === element ? 'selectedOption' : null}>{element}</li>
                ))}
            </ul>
        </div>
    )
}