import React, { useState, useLayoutEffect, useRef } from "react";
import "./style.css";

function DropDownElemento({ selected, items }) {
  const varivel = useRef(null);
  let styleDinamicDiv;
  const [open, setOpen] = useState(false);
  const [chosen, setChosen] = useState(selected);

  useLayoutEffect(() => {
    styleDinamicDiv = document.getElementsByClassName("style--dinamicDiv")[0];
  });

  function closeDropDown() {
    if (styleDinamicDiv) {
      styleDinamicDiv.style.height = "0px";
    }
    setOpen(false);
  }

  function openDropDown() {
    styleDinamicDiv.style.height = items.length * 32 + "px";
    setOpen(true);
  }

  function DropDownMenu() {
    open ? closeDropDown() : openDropDown();
  }

  function optionSelected(element) {
    setChosen(element);
    closeDropDown();
  }

  function clickOutDropDown(event) {
    if (varivel.current && !varivel.current.contains(event.target)) closeDropDown();
  }

  document.addEventListener("mousedown", clickOutDropDown);

  return (
    <div className={`dropdown ${open ? "up" : null}`}>
      <div ref={varivel}>
        <button className="button" onClick={DropDownMenu}>
          <div>{chosen}</div>
          <div className="chevron"> </div>
        </button>
        <ul className="style--dinamicDiv">
          {items.map(element => (
            <li
              onClick={() => {
                optionSelected(element);
              }}
              className="p--item"
            >
              {element}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default DropDownElemento;
