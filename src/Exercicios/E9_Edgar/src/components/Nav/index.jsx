import React, { useLayoutEffect } from "react";
import { Link } from "react-router-dom";
import "./style.css";

export default function Nave(props) {
    useLayoutEffect(() => {
        const BarraVem = document.getElementsByClassName("BarraVem")[0];
        const BigBar = document.getElementsByClassName("BigBar")[0];

        const position = `${BarraVem.offsetLeft}px`;
        const tamanho = `${BarraVem.offsetWidth}px`;

        BigBar.style.left = position;
        BigBar.style.width = tamanho;
    });

    return (
        <div className="NavContentor">
            <div className="BigBar"></div>
            <nav>
                <ul>
                    <li>
                        <Link to="/" className={props.page === "home" ? "BarraVem" : null}>Home</Link>
                    </li>

                    <li>
                        <Link to="/imagens" className={props.page === "imagens" ? "BarraVem" : null}>Imagens</Link>
                    </li>

                    <li>
                        <Link to="/sobre" className={props.page === "sobre" ? "BarraVem" : null}>Sobre</Link>
                    </li>

                    <li>
                        <Link to="/dropDown" className={props.page === "dropDown" ? "BarraVem" : null}>DropDown</Link>
                    </li>

                    <li>
                        <Link to="/banner" className={props.page === "banner" ? "BarraVem" : null}>Banner</Link>
                    </li>

                    <li>
                        <Link to="/tabs" className={props.page === "tabs" ? "BarraVem" : null}>Tabs</Link>
                    </li>

                    <li>
                        <Link to="/fetch" className={props.page === "fetch" ? "BarraVem" : null}>Fetch</Link>
                    </li>

                    <li>
                        <Link to="/reduxExercise" className={props.page === "reduxExercise" ? "BarraVem" : null}>ReduxExercise</Link>
                    </li>
                </ul>
            </nav>
        </div>
    )
}