import React, { useLayoutEffect, useState } from "react";
import "./style.css";

function BannerComp(props) {
    let bannerContentor;
    const [banner, setBanner] = useState(false);

    useLayoutEffect(() => {
        bannerContentor = document.getElementsByClassName("bannerContentor")[0];
        document.getElementsByClassName("alert")[0].style.backgroundColor = props.bgcolor;
    });

    function bannerUp() {
        bannerContentor.style.top = "-200px";
        setBanner(true);
        setTimeout(() => {
            banerDown();
        }, 2000);
    }

    function banerDown() {
        bannerContentor.style.top = "100px";
        setBanner(false);
    }

    function bannerAnimation() {
        banner ? banerDown() : bannerUp();
    }

    return (
        <div className="bannerContentor">
            <div className="alert">
                {props.children}
                <span className="closebtn" onClick={bannerAnimation}>&times;</span>
            </div>
        </div>
    )
}
export default BannerComp; 