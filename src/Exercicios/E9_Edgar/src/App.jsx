import React from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";

import Nave from "./components/Nav"
import Home from "./pages/Home/Home";
import Imagens from "./pages/Imagens";
import Sobre from "./pages/Sobre/Sobre";
import DropDown from "./pages/DropDown";
import Banner from "./pages/Banner";
import Tabs from "./pages/Tabs";
import Fetch from "./pages/Fetch";
import ReduxExercise from "./pages/ReduxExercise";
// import FilterPokemon from "./pages/FilterPokemon";

import "./style/reset.css";

export default function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <Nave page="home" />
          <Home />
        </Route>

        <Route path="/imagens">
          <Nave page="imagens" />
          <Imagens />
        </Route>

        <Route path="/sobre">
          <Nave page="sobre" />
          <Sobre />
        </Route>

        <Route path="/dropdown">
          <Nave page="dropDown" />
          <DropDown />
        </Route>

        <Route path="/banner">
          <Nave page="banner" />
          <Banner />
        </Route>

        <Route path="/tabs">
          <Nave page="tabs" />
          <Tabs />
        </Route>

        <Route path="/fetch">
          <Nave page="fetch" />
          <Fetch />
        </Route>

        <Route path="/reduxExercise">
          <Nave page="reduxExercise" />
          <ReduxExercise />
        </Route>

        {/* <Route path="/filerPokemon">
          <Nave page="filerPokemon" />
          <FilterPokemon />
        </Route> */}
      </Switch>
    </BrowserRouter>
  );
}