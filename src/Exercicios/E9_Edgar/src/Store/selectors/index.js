import { createSelector } from "reselect";

export const getCurrentPage = (state) => state.url?.current;

export const getPokemonID = createSelector(
    getCurrentPage,
    page => {
        const url = page;
        if (!url) return null;
        const urlSplitted = url.split("/");
        return urlSplitted[1] === "reduxexercise" ? urlSplitted[2] : null;
    });

export const getPokemons = state => state.pokemonList;

export const getPokemonById = createSelector(
    getPokemons,
    getPokemonID,
    (pokemons, id) => {
        const pokemonKey = Object.keys(pokemons).find(pokemon =>
            pokemons[pokemon].id === id)
        return pokemonKey ? pokemons[pokemonKey] : null
    }

);

