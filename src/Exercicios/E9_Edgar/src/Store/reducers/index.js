import { ADD_POKEMON, USER_NAVIGATION } from "../constants";

const initialState = {
    url: { current: null, previous: null },
    pokemonList: {}
};

export function pokemonReducer(state = initialState, action) {
    switch (action.type) {
        case ADD_POKEMON:
            return {
                ...state,
                pokemonList: {...state.pokemonList, [action.name]: action.pokemon }
            };
        case USER_NAVIGATION:
            return {
                ...state,
                url: {
                    current: action.url,
                    previous: state.url.current
                }
            };
        default:
            return state;
    }
}