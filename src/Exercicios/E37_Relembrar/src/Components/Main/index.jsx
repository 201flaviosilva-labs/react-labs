import React from 'react';
import { Switch, Route, } from "react-router-dom";

import Home from "../../Pages/Home";
import Form from "../../Pages/Form";
import Todo from "../../Pages/Todo";
import Movies from "../../Pages/Movies";

export default function Main() {
	return (
		<main>
			<Switch>
				<Route exact path="/" component={Home} />
				<Route path="/form" component={Form} />
				<Route path="/todo" component={Todo} />
				<Route path="/movies" component={Movies} />
			</Switch>
		</main>
	)
}
