import React from 'react';
import NavBar from "../NavBar";

export default function Header() {
	return (
		<header>
			<h1>React App</h1>
			<NavBar />
		</header>
	)
}
