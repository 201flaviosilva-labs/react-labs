import React, { useEffect, useState } from "react";
import Label from "../../Components/Label";
import Input from "../../Components/Input";

export default function Movies() {
	const styles = {
		display: "flex",
		alignItems: "center"
	};
	const styleSpanMovies = { fontHeight: "bold" };

	const [listMovies, setListMovies] = useState([]);
	const [search, setSearch] = useState("Pirates of Caribbean");

	useEffect(() => {
		fetch(`https://itunes.apple.com/search?term=${search}&entity=movie`)
			.then((response) => response.json())
			.then((data) => setListMovies(data.results));
	}, [search]);

	return (
		<>
			<h2>Movies</h2>
			<Label>
				<Input
					placeholder="Write a Movie"
					value={search}
					onChange={(e) => setSearch(e.target.value)}
				/>
			</Label>
			<ul>
				{listMovies.map(m => <li style={styles} key={Math.floor(Math.random() * 10000)}>
					<img src={m.artworkUrl100} alt="Movie Img" />
					<div>
						<p style={styles}><span style={{ color: "Red" }}> Nome: </span> <span style={styleSpanMovies}>{m.trackName}</span></p>
						<p style={styles}><span style={{ color: "Green" }}>Artista: </span> <span style={styleSpanMovies}>{m.artistName}</span></p>
						<p style={styles}><span style={{ color: "Blue" }}>Genero: </span> <span style={styleSpanMovies}>{m.primaryGenreName}</span></p>
					</div>
				</li>
				)}
			</ul>
		</>
	)
}
