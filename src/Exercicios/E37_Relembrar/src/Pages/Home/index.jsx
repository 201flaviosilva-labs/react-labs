import React from 'react';
import Counter from "../../Components/Counter";
import Height from "../../Components/Height";

export default function Home() {

	return (
		<>
			<Counter />
			<hr />
			<Height />
		</>
	);
}
