import React from 'react';
import { useState } from 'react';

function App() {
  const [count, setCount] = useState(0);
  const [name, setName] = useState('');
  return (
    <div className="App">
      <h1>{count}</h1>
      <button onClick={() => setCount(antigo => antigo + 1)}>Por</button>
      <button onClick={() => setCount(antigo => antigo - 1)}>Tirar</button>
      <hr />
      <label className="header-name">
        <input
          value={name}
          onChange={e => setName(e.target.value)}
          placeholder="Nada"
        />
      </label>
      <p>{name}</p>
    </div>
  );
}

export default App;
