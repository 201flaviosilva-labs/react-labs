import React, { Component } from "react";

class App extends Component{
  constructor(){
    super();
    this.state={
      count:0
    }
    this.handleClick = this.handleClick.bind(this); // Ainda não sei porque
  }

  handleClick(){
    this.setState(antigo=>{
      console.log(this.state.count);
    return{
      count: antigo.count +1
    }})
  }
  render(){
    return(
      <>
        <h1>Counter: {this.state.count}</h1>
        <button type="button" onClick={() => this.handleClick()}>Click</button>
      </>
    )
  }
}
export default App;