import React, { Component } from 'react';

class App extends Component{
  constructor(){
    super();
    this.state = {
      count:0

    }
    this.handleClick = this.handleClick.bind(this); // Ainda não percebi, mas se não tiver isto, dá erro
  }


  handleClick(){
    // this.setState({count: 1}); // O número fica sempre igual, muda o estádo, mas como é o mesmo número, continua igual
    this.setState( anterior => {
      return{
        count: anterior.count + 1
      }
    } )
  }


  render(){
    return(
      <div className="App">
          <h1>Counter: {this.state.count}</h1>
          <button type="button" onClick={() => this.handleClick()}>Click</button>      
      </div>
    )
  }
}

export default App;
