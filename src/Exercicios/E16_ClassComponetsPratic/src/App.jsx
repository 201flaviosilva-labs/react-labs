import React, { Component } from 'react';
import CompClass from './components/CompClass';
import CompFunc from './components/CompFunc';
import Greeting from './components/Greeting';
import Header from './components/Header';


class App extends Component{
  render(){
    return(
      <>
        <Header nome="Flávio"/>
        <Greeting />
      </>
    )
  }
}


// function App() {
//   return (
//     <div className="App">
//      <Header nome="Flávio"/>
//      <Greeting />
//     </div>
//   );
// }

export default App;