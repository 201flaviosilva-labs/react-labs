import React, { Component } from "react";


class Greeting extends Component{
    render(){
        const horas = new Date().getHours();
        const minutos = new Date().getMinutes();
        const tempo = horas < 12 ? "Bom Dia" : horas > 20 ? "Boa tarde" : "Boa Noite";

        return(
        <h2>{tempo + " " + horas + ":" + minutos}</h2>
        )
    }
}

export default Greeting;