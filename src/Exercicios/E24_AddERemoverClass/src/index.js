import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import cx from "classnames";
import "./styles.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { count: 100, faill:"" };
    this.liked = false;
  }

  like = () => {
    fetch("http://localhost:3000/")
    .then(data => data.json())
    .then(res => {
      if(res){
          this.setState({faill:""});
          console.log(res);
          if (!this.liked) {
            this.liked = true;
            this.setState(({ count }) => ({ count: count + 1 }));
            return;
          }
          this.liked = false;
          this.setState(({ count }) => ({ count: count - 1 }));
    }else{
          this.setState({faill:"Algo deu errado! :)"});
    }
  });
  };

  render() {
    const { count,faill } = this.state;
    return (
      <Fragment>
        <h1> {faill} </h1>
        <button
          type="button"
          onClick={this.like}
          className={cx("like-button", { liked: this.liked })}>
          Like | <span className="likes-counter">{count}</span>
        </button>
      </Fragment>
    );
  }
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
