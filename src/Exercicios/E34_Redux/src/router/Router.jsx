import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import store from "../store";

import Home from '../pages/home';
import About from '../pages/About';

function Router() {
  return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/about" component={About} />
            </Switch>
        </BrowserRouter>
  );
}

export default Router;
