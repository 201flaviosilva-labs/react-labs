import React from 'react';
import Couter from '../../components/couter';

function Home() {
  return (
    <>
      <h1>Our Redux counter</h1>
      <Couter />
    </>
  );
}

export default Home;
