import React from 'react';
import Buttons from '../buttons';
import {connect} from 'react-redux';

function Couter(props) {
  console.log(props);
  return (
    <div>
        <p>couter value:</p><span>{props.couter}</span>
        <Buttons />
    </div>
  );
}

const mapStateToProps = (state) =>({
  couter: state.couter
})

export default connect(mapStateToProps)(Couter);