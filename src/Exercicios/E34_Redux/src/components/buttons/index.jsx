import React from 'react';
import {connect} from 'react-redux';

import {increaseCounter, decreaseCounter} from "../../store/counter/actions";

function Buttons(props) {
  console.log(props);
  return (
    <div>
        <button
        onClick= {() =>{
          props.increaseCounter();
        }}
      >Add ({ props.couter + 1})</button>

        <button
          onClick={() => {
              if ( props.couter !==0 ) {
                props.decreaseCounter();
              }
            }
          }>
          Not Add ({ props.couter !== 0  ? props.couter - 1 : 0})</button>
    </div>
  );
}

const mapStateToProps = (state) =>({
  couter: state.couter
})

const mapDispatchToProps = {
  increaseCounter : increaseCounter,
  decreaseCounter : decreaseCounter
}


export default connect(mapStateToProps, mapDispatchToProps)(Buttons);