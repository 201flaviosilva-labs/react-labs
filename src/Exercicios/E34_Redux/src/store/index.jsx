import {createStore} from 'redux';
import couterRedux from './counter/reducer';
import {composeWithDevTools} from 'redux-devtools-extension';

const devTools = composeWithDevTools();

const store = createStore(couterRedux, devTools);

export default store;
