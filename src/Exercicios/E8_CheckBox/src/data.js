const data = [{
        id: 1,
        text: "Html",
        complited: true
    },
    {
        id: 2,
        text: "CSS",
        complited: false
    },
    {
        id: 3,
        text: "JavaScript",
        complited: false
    },
    {
        id: 4,
        text: "React",
        complited: true
    }
]
export default data;