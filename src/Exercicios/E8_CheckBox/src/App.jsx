import React, { Component } from 'react';
import MyTodoItem from "./components/TodoItem";
import "./App.scss";
import data from "./data";


class App extends Component{
  constructor(){
    super();
    this.state ={
      todos:data
    }
  }
  render(){
    const newData = this.state.todos.map(e => <MyTodoItem key={e.id} item={e}/>);
    return(
      <>
        {newData}
      </>

    )
  }
}
export default App;
