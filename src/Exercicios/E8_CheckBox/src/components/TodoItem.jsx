import React from "react";

function MyTodoItem( props){
    console.log(props);
    return(
        <div className="itens">
            <input
            type="checkbox"
            name="Programacao"
            className="ImputCheckbox"
            checked={props.item.complited}
            onChange={()=>console.log("Changed!")}
            />
            <span>{props.item.text}</span>
        </div>
    )
}

export default MyTodoItem;