import React, { Component } from "react";

class App extends Component{
  constructor(){
    super();
  }
  render(){
    return(
      <>
        <img src="https://i.imgur.com/pfqLYFI.gif" alt="Gif" onMouseMove={() => MyFunc()}/>
        <br/>
        <br/>
        <button type="button">Click Me</button>
      </>
    )
  }
}


function MyFunc(){
  console.log("Função de fora");
}

export default App;