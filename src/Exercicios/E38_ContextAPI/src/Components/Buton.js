import React from "react";

import { useCount } from "../State/Count";

export default function Buton() {
	const { count, setCount } = useCount();
	return (
		<>
			<button onClick={() => setCount(count + 1)}><span>{count}</span> +1 </button>
		</>
	)
}
