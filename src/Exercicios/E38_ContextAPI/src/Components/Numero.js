import React from "react";

import { useCount } from "../State/Count";

export default function Numero() {
	const style = { background: "black" };
	return (
		<>
			<div style={style}>
				<NumeroComp />
			</div>
		</>
	)
}

function NumeroComp() {
	const { count } = useCount();
	const style = { color: "white" };
	return (
		<>
			<p style={style}>{count}</p>
		</>
	)
}
