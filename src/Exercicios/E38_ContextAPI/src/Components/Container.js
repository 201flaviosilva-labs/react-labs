import React from "react";

import Buton from "./Buton";
import Numero from "./Numero";

export default function Container() {
	return (
		<div>
			<Numero />
			<Buton />
		</div>
	)
}
