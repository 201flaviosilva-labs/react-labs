import React from 'react';

import CountProvider from "./State/Count";

import Container from "./Components/Container";

export default function App() {
  return (
    <>
      <CountProvider>
        <Container />
      </CountProvider>
    </>
  );
}

