import React from 'react';
import {Torre} from "./assets/Torre.png";
function App() {
  return (
    <div className="App">
      <img src={Torre} alt="Torre"/>
    </div>
  );
}

export default App;
