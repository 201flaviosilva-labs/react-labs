import React from "react";
import Image from "./Image";
import Name from "./Name";
import Phone from "./Phone";
import Email from "./Email";

function ContactCard (props){
    console.log(props);
    return(
        <div>
            {/* <div>
                <img src={props.imgUrl} alt="AC Black Flag"/>
                <h3>{props.name}</h3>
                <p>Phone: {props.phone}</p>
                <p>Email: {props.email}</p>
            </div> */}

            <Image imgUrl={props.imgUrl}/>
            <Name name={props.name}/>
            <Phone phone={props.phone}/>
            <Email email={props.email}/>
        </div>
    )
}

export default ContactCard;