import React from "react";

function Phone (props){
    return(
        <p>Phone: {props.phone}</p>
    )
}

export default Phone;