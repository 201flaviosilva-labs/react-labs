import React from "react";

function Image (props){
    return(
        <img src={props.imgUrl} alt=""/>
    )
}

export default Image;