import React from 'react';
import ContactCard from './components/ContactCard';

function App() {
  return (
    <div className="App">
      <ContactCard 
          imgUrl="https://media.giphy.com/media/aFTt8wvDtqKCQ/giphy.gif"  
          name="Mr .Whiskerson" 
          phone="(212) 555-1234" 
          email="mr.whiskerson@catnap.moew"
          />

      <ContactCard 
          imgUrl="https://media.giphy.com/media/85p0dpfRvPnfa/giphy.gif"  
          name="Gato 2" 
          phone="1234" 
          email="gato2@gmail.com"
          />

      <ContactCard 
          imgUrl="https://media.giphy.com/media/13BwjdpxACoBPO/giphy.gif"  
          name="Gato 3" 
          phone="4321" 
          email="gato3@gmail.com"
          />

      <ContactCard 
          imgUrl="https://media.giphy.com/media/8R7GUZWaydcOY/giphy.gif"  
          name="gato 4" 
          phone="567890" 
          email="gato4@gmail.com"
          />
    </div>
  );
}

export default App;
