import React, { Component } from "react";
// import RegisterList from "./RegisterList";

class RegisterAccount extends Component{
    constructor(){
        super();
        this.state={
            balance:0,
            id:0,
            addTitle:"",
            addValue:0,
            list:[]
        }
    }
    render(){
        const {balance, addTitle, addValue, list} = this.state;
        return(
            <>
                <h1>Saldo: {balance} </h1>
                <form onSubmit={event => this.handleSubmit(event)}>
                    <input
                    type="text"
                    placeholder="Title"
                    name="addTitle"
                    value={addTitle}
                    onChange={event => this.handleChangeTitle(event)}
                    />

                    <br/>

                    <input
                    type="number"
                    placeholder="Value"
                    name="addValue"
                    value={addValue}
                    onChange={event => this.handleChangeValue(event)}
                    />

                    <br/>

                    <button type="submit">Submit</button>
                </form>


                {/* <RegisterList list={list} /> */}

            <hr/>
          </>
        )
    }
    handleChangeTitle(event) {
        this.setState({ addTitle: event.target.value });
    }
    handleChangeValue(event) {
        this.setState({ addValue: event.target.value });
    }

    handleSubmit(event){
        event.preventDefault();
        this.setState(state => ({
            id: Number(state.id) + 1,
            balance: Number(state.balance) + Number(state.addValue),
            addTitle: "",
            addValue: 0,
            list: state.list.concat([{
                id: state.id,
                title: state.addTitle,
                value: state.addValue
            }])
        }));
        // console.log(this.state.id);
    }
}


export default RegisterAccount;



// 