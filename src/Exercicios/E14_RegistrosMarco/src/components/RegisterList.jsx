import React, { Component } from "react";
import RegisterItem from "./RegisterItem";

class RegisterList extends Component{
    constructor(props){
        super(props);
        this.state ={
            todos: props.list
          }
          console.log("Props: " + props);
          console.log("Todos:" + this.state.todos);
    }

    render(){
        const registro = this.state.todos.map(e => <RegisterItem key={e.id} item={e}/>);
        return(
            <ul>
                {registro}
            </ul>
        )
    }
}

export default RegisterList;