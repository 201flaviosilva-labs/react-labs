import React from 'react';
import RegisterAccount from './components/RegisterAccount';
import "./Style/style.css";

function App() {
  return (
    <div className="App">
      <RegisterAccount />
    </div>
  );
}

export default App;
