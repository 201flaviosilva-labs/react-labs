import React, { Component } from "react";
import "../../Style/footer.css"

class Footer extends Component{
    render(){
        return(
            <footer>
                <p>©2019 Apponboard, Inc<a href="https://www.buildbox.com/privacy-policy/">Privacy Policy </a><a href="https://www.buildbox.com/terms/">Terms</a></p>
            </footer>
        )
    }
}
export default Footer;