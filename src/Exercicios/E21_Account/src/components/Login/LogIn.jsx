import React, { Component } from "react";
import Header from "../header/Header";
import MainLogin from "./MainLogIn";
import Footer from "../footer/Footer";

class LogIn extends Component{
    render(){
        return(
            <>
                <Header />
                <MainLogin />
                <Footer />
            </>
        )
    }
}
export default LogIn;