import React, { Component } from "react";
import "../../Style/footer.css";

class Header extends Component{
    render(){
        return(
            <header>
                <a href="https://www.buildbox.com/"><img src="https://accounts.buildbox.com/assets/images/LogoBB_Normal.png" alt="BuuilBox Logo"/></a>
                <nav>
                    <ul>
                        <li><a href="https://www.buildbox.com/blog/">Blog</a></li>
                        <li><a href="https://www.buildbox.com/forum/index.php">Forums</a></li>
                        <li><a href="https://discordapp.com/invite/buildbox/">Discord</a></li>
                        <li><a href="http://help.buildbox.com/en/">Knowledge Base</a></li>
                        <li><a href="google.com">Sign In</a></li>
                    </ul>
                </nav>
            </ header>
        )
    }
}
export default Header;