import React from 'react';

const ViewName = (props) => {
    return(
        <div>
          <h2>Your name is: {props.name}</h2>
        </div>
    );
}

export default ViewName;