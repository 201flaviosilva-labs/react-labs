import React, { useState } from "react";
import Input from './Input';
import ViewName from './ViewName';

function App() {
    const [name, setName] = useState("");
    
    return ( 
        <div>
            <Input 
                handleSetName={(event) => {
                    setName(event.target.value);
                }} 
            />
            <ViewName
                name={name}
            />
        </div>
    );
}

export default App;