import React, { Component } from "react";

class Input extends Component {
  render() {
    return (
      <div>
        Enter your name <input type="text" onChange={this.props.handleSetName} />
      </div>  
    );
  }
}

export default Input;