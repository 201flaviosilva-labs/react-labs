import React, { useEffect, useRef, useState, memo, useCallback } from "react";
import "./styles.css";

export default function App() {
  const [transactions, setTransactions] = useState([
    { id: 42, name: "cenas", description: "whatever" },
    { id: 56, name: "cenas", description: "whatever" },
  ]);

  const [selectedTransaction, setSelectedTransaction] = useState(null);

  function handleSave(updatedtransaction) {
    setSelectedTransaction(null);
    setTransactions((transactions) =>
      transactions.map((t) =>
        t.id === updatedtransaction.id ? updatedtransaction : t
      )
    );
  }

  return (
    <div>
      <ul>
        {transactions.map((t) => (
          <li key={t.id} onClick={() => setSelectedTransaction(t)}>
            {t.id} {t.name} {t.description}
          </li>
        ))}
      </ul>
      // ----
      <Modal isOpen={selectedTransaction !== null}>
        <TransactionModal
          transaction={selectedTransaction}
          onSave={handleSave}
        />
      </Modal>
      // ----
    </div>
  );
}

// ------------------------------------------------------------------------------------

function Modal({ isOpen, children }) {
  return (
    <div style={{ transition: "all 1s", opacity: isOpen ? "1" : "0" }}>
      {children}
    </div>
  );
}

//   -------------------------------------------------------------------------------------

function TransactionModal({ transaction, onSave }) {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");

  useEffect(() => {
    if (transaction) {
      setName(transaction.name);
      setDescription(transaction.description);
    }
  }, [transaction]);

  function handleSubmit(e) {
    e.preventDefault();

    onSave({ id: transaction.id, name, description });
  }

  return (
    <form onSubmit={handleSubmit}>
      <label>
        name{" "}
        <input
          name="name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </label>
      <label>
        description{" "}
        <input
          name="description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
      </label>
      <button>save</button>
    </form>
  );
}
