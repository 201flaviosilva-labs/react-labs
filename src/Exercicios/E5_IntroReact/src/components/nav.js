import React, { Fragment } from "react";
import {Link} from "react-router-dom";


export default function Nave (props){
    return(
        <Fragment>
            <div className="BigBar"></div>
            <div className="BarMove"></div>
            <ul>
                <li>
                    <Link to="/" className={props.page==="Home"}>Home</Link>
                </li>
                <li>
                    <Link to="/about" className={props.page==="About"}>About</Link>
                </li>
                <li>
                    <Link to="/dashboard" className={props.page==="Dashboard"}>Dashboard</Link>
                </li>
            </ul>
        </Fragment>
    )}