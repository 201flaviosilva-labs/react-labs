import React, { Component} from "react";
import {hot} from "react-hot-loader";
import "./App.less";
import BasicExample from "./components/Router";

class App extends Component{
  render(){
    return(
      <div className="App">
       <BasicExample />
      </div>
    );
  }
}

export default hot(module)(App);