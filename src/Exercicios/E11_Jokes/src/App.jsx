import React from 'react';
import Jokes from "./components/Jokes";
import jokesData from "./jokesData";

function App() {
    const newArrayJokes=  jokesData.map(e =><Jokes key={e.id} question={e.question} answer={e.answer}/>)

    return ( 
    <div className = "App">
        
        {newArrayJokes}

       {/* <Jokes question="Nome" answer="Silva"/>
       <Jokes  answer="Silva"/>
       <Jokes question="Nome"/>
       <Jokes question="Idade" answer="12"/> */}
    </div>
    );
}

export default App;