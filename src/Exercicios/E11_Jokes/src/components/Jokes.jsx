import React from 'react';

function Jokes(props) {
    return ( 
    <div className = "App" >
        <h3 style={{display: !props.question && "none"}}>Question: {props.question}?</h3>
        <h3 style={{color: !props.answer && "#888"}}>Answer: {props.answer}!</h3>
        <hr/>
    </div>
    );
}

export default Jokes;