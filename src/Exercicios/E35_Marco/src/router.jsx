import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Header from "./Components/Header";

import Home from "./Pages/Home";
import Gallery from "./Pages/Gallery";
import About from "./Pages/About";
import Contact from "./Pages/Contact";
import LogIn from "./Pages/LogIn";

export default function Rooter() {
    return (
        <BrowserRouter>
            <Header />
            <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/Gallery" component={Gallery} />
                <Route path="/About" component={About} />
                <Route path="/Contact" component={Contact} />
                <Route path="/LogIn" component={LogIn} />
            </Switch>
        </BrowserRouter>
    )
}