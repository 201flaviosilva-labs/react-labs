import React from 'react';
import Rooter from "./router";

import "./Style/reset.css";


function App() {
  return (
    <div className="App">
      <Rooter />
    </div>
  );
}

export default App;
