import React from "react";
import { Link } from "react-router-dom";
import "./style.css";

export default function Header() {
    return (
        <header>
            <nav>
                <ul>
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/Gallery">Gallery</Link></li>
                    <li><Link to="/About">About</Link></li>
                    <li><Link to="/Contact">Contact</Link></li>
                    <li><Link to="/LogIn">LogIn</Link></li>
                </ul>
            </nav>
        </header>
    )
}