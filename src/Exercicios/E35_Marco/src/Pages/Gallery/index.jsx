import React from "react";
import MSchool from "../../Assets/img/MSchool.jpg";
import Galo from "../../Assets/img/Galo.svg";
import "./style.css";

export default function Galleery() {
    return (
        <div className="GalleeryContainer">
            <h2>Gallery</h2>
            <div className="ImgsConjunto">
                <img src="https://picsum.photos/200/300" alt="Imagem" />
                <img src="https://picsum.photos/200" alt="Imagem" />
                <img src={MSchool} alt="Mindera School" />
                <img src={Galo} alt="Jogo do Galo Animado" />
            </div>
        </div>
    )
}