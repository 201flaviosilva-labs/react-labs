import React, { useState } from "react";
import "./style.css";

const vUsername = "a";
const vPassword = "1";

export default function LogIn() {
    const [username, setUserName] = useState("");
    const [password, setpassword] = useState("");

    function handleSubmit(event){
        event.preventDefault();

        if(!username && !password){
            return;
        }else if (username === vUsername && password === vPassword) {
            console.log("username + password");
        }
    }

    return (
        <div className="LogInContainer">
            <div className="Form">
                <h2>LogIn</h2>
                <form onSubmit={()=>handleSubmit}>
                    <input type="text" placeholder="User Name" value={username} onChange={event => setUserName(event.target.value)}/>
                    <input type="password" placeholder="Password" className="Password" value={password} onChange={event => setpassword(event.target.value)}/>
                    <button type="submit">Submit</button>
                </form>
            </div>
        </div>
    )
}