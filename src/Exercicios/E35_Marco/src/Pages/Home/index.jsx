import React from "react";
import "./style.css";

export default function Home() {
    return (
        <div className="HomeContainer">
            <h2>Home</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi consequuntur unde, hic deleniti itaque aliquam dolores voluptas id, culpa quaerat pariatur ab dignissimos qui ea illum. Totam cumque minus consectetur.</p>
        </div>
    )
}