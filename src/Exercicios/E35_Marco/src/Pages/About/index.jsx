import React from "react";
import "./style.css";

export default function About() {
    return (
        <div className="AboutContainer">
            <h2>About</h2>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quidem ducimus nam fuga. A illum esse dolore cupiditate vero commodi quam dolores nam architecto hic consequuntur enim, atque, nostrum nobis animi.</p>
            <img src="https://media.giphy.com/media/Nof2c4lu4nYt2/giphy.gif" alt="Iamgem" />
        </div>
    )
}