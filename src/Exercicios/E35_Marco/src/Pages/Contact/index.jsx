import React, { useState } from "react";
import swal from 'sweetalert';
import "./style.css";

export default function Contact() {
    const [email, setEmail] = useState("");
    const [subject, setSubject] = useState("");
    const [message, setMessage] = useState("");

    function handleSubmit(event) {
        event.preventDefault();
        swal("Good job!", "Email has been sent!", "success");
        console.log(email);
        console.log(subject);
        console.log(message);
    }

    return (
        <div className="ContactContainer">
            <h2>Contact</h2>
            <form onSubmit={handleSubmit}>
                <input type="email" value={email} onChange={event => setEmail(event.target.value)} placeholder="Email" />
                <input type="text" value={subject} onChange={event => setSubject(event.target.value)} placeholder="Subject" />
                <textarea placeholder="Message" value={message} onChange={event => setMessage(event.target.value)}></textarea>
                <button type="submit">Submit</button>
            </form>
        </div>
    )
}