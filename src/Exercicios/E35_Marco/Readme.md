- Create a react app that has 4 pages which are: 

		- an Home page
		- an Gallery page
		- an About page
		- an Contact page

	- The content of each page:

		- HomePage - Should have some random text entered at middle
		- GalleryPage - Should have 4 random images entered at middle vertically aligned
		- AboutPage - Should have some random text with an image
		- ContactPage - Should have a simple form with the following fields:
		
				- Email
				- Subject
				- Message
				- Submit button

			When I submit the form I want to receive fake alert that email has been sent
	- All pages should have an header that has the navigation so the user can go to each page