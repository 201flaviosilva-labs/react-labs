import React from "react";
import { Link } from "react-router-dom";

import "./style.css";

export default function Nave(props) {
    return (
        <nav>
            <ul>
                <li className={props.page === "Home" ? "Selected" : null}><Link to="/">Home</Link></li>
                <li className={props.page === "Gallery" ? "Selected" : null}><Link to="/Gallery">Gallery</Link></li>
                <li className={props.page === "DropDown" ? "Selected" : null}><Link to="/DropDown">DropDown</Link></li>
                <li className={props.page === "Banner" ? "Selected" : null}><Link to="/Banner">Banner</Link></li>
                <li className={props.page === "Fetch" ? "Selected" : null}><Link to="/Fetch">Fetch</Link></li>
                <li className={props.page === "Google" ? "Selected" : null}><Link to="/Google">Goole</Link></li>
                <li className={props.page === "FollowMe" ? "Selected" : null}><Link to="/FollowMe">FollowMe</Link></li>
            </ul>
        </nav>
    )
}