import React from 'react';
import Router from './Route/router';

import './Styles/style.min.css';

function App() {
  return (
    <div className="App">
      <Router />
    </div>
  );
}

export default App;
