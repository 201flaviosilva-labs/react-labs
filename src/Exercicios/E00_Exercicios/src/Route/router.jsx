import React from "react";
import {BrowserRouter, Switch, Route} from "react-router-dom";

import Nave from "../Components/Nave";

import Home from "../Pages/Home";
import Gallery from "../Pages/Gallery";
import DropDown from "../Pages/DropDown";
import Banner from "../Pages/Banner";
import Fetch from "../Pages/Fetch";
import Google from "../Pages/Google";
import FollowMe from "../Pages/FollowMe";

export default function Router(){
    return(
        <>
            <BrowserRouter>
                <Switch>
                    <Route exact path="/">
                        <Nave page = "Home" />
                        <Home />
                    </Route>

                    <Route path="/Gallery">
                        <Nave page = "Gallery" />
                        <Gallery />
                    </Route>

                    <Route path="/DropDown">
                        <Nave page = "DropDown" />
                        <DropDown />
                    </Route>

                    <Route path="/Banner">
                        <Nave page = "Banner" />
                        <Banner />
                    </Route>

                    <Route path="/Fetch">
                        <Nave page = "Fetch" />
                        <Fetch />
                    </Route>

                    <Route path="/Google">
                        <Nave page = "Google" />
                        <Google />
                    </Route>

                    <Route path="/FollowMe">
                        <Nave page = "FollowMe" />
                        <FollowMe />
                    </Route>
                </Switch>
            </BrowserRouter>
        </>
    )
}