import React from "react";
import "./style.css";
import Imagem from "../../Assets/Imagem.jpg";

export default function Gallery() {
    return (
        <div className="GalleryContentor">
            <h1>Gallery</h1>
            <div>
                <h2>Importada da Net</h2>
                <img src="https://picsum.photos/200/300" alt="Img" />
            </div>
            <div>
                <h2>Ficheiro local</h2>
                <img src={Imagem} alt="Img" />
            </div>
        </div>
    )
}