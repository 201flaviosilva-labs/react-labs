import React from "react";
import { Link } from "react-router-dom";
import {FaEdit} from "react-icons/fa";

export default function ProfileComp(){
    return(
        <div className="ProfileCompContentor">
            <h3>AAA</h3>
            <img src="https://picsum.photos/200/300" alt="Imagem"/>
            <Link to="/EditProfile">
                <FaEdit size={16} color="gray"/> Edit
            </Link>
        </div>
    )
}