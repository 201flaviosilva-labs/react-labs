import React from "react";
import {ProfileComp} from "./ProfileComp";


export default function Profiles(){
    return(
        <div className="ProfilesContentor">
            <h1>Profiles</h1>
            <section>
                <article>
                    <ProfileComp />
                </article>
            </section>

        </div>
    )
}