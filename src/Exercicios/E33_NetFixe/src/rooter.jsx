import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Home from "./Pages/Home/";
import LogIn from "./Pages/LogIn/";
import Profiles from "./Pages/Profiles/";
import EditProfile from "./Pages/EditProfile/";

export default function Rooter(){
    return(
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/LogIn" component={LogIn}/>
                <Route path="/Profiles" component={Profiles}/>
                <Route path="/EditProfile" component={EditProfile}/>
            </Switch>
        </BrowserRouter>
    )
}