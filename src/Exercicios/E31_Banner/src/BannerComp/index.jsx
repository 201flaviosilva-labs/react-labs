import React, { useLayoutEffect, useState } from 'react';
import "./style.css";

function BannerComp() {
  let alertDiv;
  const [banner, setBanner] = useState(false);
  const [tipoErro, setTipoErro] = useState("Email");

  useLayoutEffect(() => {
    alertDiv = document.getElementsByClassName("alertDiv")[0];
  });
    
  function bannerUp() {
    alertDiv.style.top = "-200px";
    setBanner(false);
  }

  function banerDown() {
    alertDiv.style.top = "200px";
    setTimeout(() => {bannerUp()}, 3000);
    setBanner(true);
  }

  function bannerAnimation() {
    setTipoErro("Olha isto é um erro!!");
    banner ? bannerUp() : banerDown();
  }

  return (
    <>
      <div className="alertDiv">
        <div className="alert">
          {tipoErro}
          <span className="closebtn" onClick={bannerUp}>&times;</span>
        </div>
      </div>
      <button onClick={bannerAnimation}>Click Para Aparecer</button>
    </>
  );
}

export default BannerComp;
