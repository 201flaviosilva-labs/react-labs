import React from 'react';
import Product from './components/Product';
import products from "./data";

function App() {
  const newProducts = products.map(e => <Product key={e.id} name={e.name} price={e.price} description={e.description} />);
  return (
    <div className="App">
      {newProducts}
    </div>
  );
}

export default App;
