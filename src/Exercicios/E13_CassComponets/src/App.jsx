import React, { Component } from 'react';

// function App() {
//   return (
//     <div className="App">

//     </div>
//   );
// }

class App extends Component{
  render() {
    const date = new Date();
    return (
      <div className="App">
        <h1>{date}</h1>
      </div>
    );
  }
}

export default App;
