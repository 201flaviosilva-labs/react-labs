import React, { Component } from "react";

class Nomes extends Component{
    constructor(props){
        super(props);
        console.log(props);
        this.state = {
            nome: this.props.nome
        }
        console.log(this.state);

        // this.handleClick = this.handleClick.bind(this);
    }


    handleClick(){
        this.setState({nome: this.props.apelido});
        console.log(this.props);
        console.log(this.state);
    }

    render(){
        return(
            <>
                <h2>Nome: {this.state.nome}</h2>
                <button type="button" onClick={() => this.handleClick()}>Update</button>
            </>
        )
    }
}

export default Nomes;