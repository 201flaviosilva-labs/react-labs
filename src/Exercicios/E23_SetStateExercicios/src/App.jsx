import React from 'react';
import Nomes from './components/Nomes';

function App() {
  return (
    <div className="App">
      <h1>Exercicio</h1>
        <Nomes nome={"Renot"} apelido={"Ferrary"} />
        <Nomes nome={"Windos"} apelido={"Mac Os"} />
        <Nomes nome={"Casa"} apelido={"Herdade"} />
        <Nomes nome={"Marta"} apelido={"Guida"} />
    </div>
  );
}

export default App;
